package com.crengland.mobile.cretoolbox.services;

import android.util.Log;

import com.crengland.mobile.cretoolbox.models.Trip;
import com.crengland.mobile.cretoolbox.models.UserLoginRequest;

import java.util.List;

import retrofit.ErrorHandler;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class AuthenticationService{
    private static final String TAG = "AuthService";
    public String LoginUser(UserLoginRequest request) throws UnauthorizedException{
        RestAdapter restAdapter = new RestAdapter.Builder()
                                    .setErrorHandler(new ServiceErrorHandler())
                                    .setEndpoint("https://dev.apps.crengland.com")
                                    .build();

        ILoginService service = restAdapter.create(ILoginService.class);

        Response response = service.Login(request);

        Log.v(TAG, "Auth request returned " + response.getStatus());

        return request.toString();
    }

    class ServiceErrorHandler implements ErrorHandler {
        @Override public Throwable handleError(RetrofitError cause) {
            Response r = cause.getResponse();
            if (r != null && r.getStatus() == 401) {
                return new UnauthorizedException();
            }
            return cause;
        }
    }

    public class UnauthorizedException extends Exception{

    }

}

package com.crengland.mobile.cretoolbox.models;


import android.util.Base64;

/**
 * Created by aresix on 11/18/14.
 */
public class UserLoginRequest {
    private String m_username;

    public String getUsername() {
        return m_username;
    }

    public String getPassword() {
        return m_password;
    }

    private String m_password;

    public UserLoginRequest(String username, String password){
        m_username = username;
        m_password = password;
    }

    @Override
    public String toString(){
        String authString = m_username + ":" + m_password;

        String authEncodedString = Base64.encodeToString(authString.getBytes(),Base64.DEFAULT);

        return "Basic " + authEncodedString;
    }


}

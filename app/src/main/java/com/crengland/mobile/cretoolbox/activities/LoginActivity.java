package com.crengland.mobile.cretoolbox.activities;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.crengland.mobile.cretoolbox.R;
import com.crengland.mobile.cretoolbox.models.UserLoginRequest;
import com.crengland.mobile.cretoolbox.services.AuthenticationService;

import roboguice.activity.RoboAccountAuthenticatorActivity;
import roboguice.activity.RoboActivity;
import roboguice.fragment.provided.RoboFragment;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

@ContentView(R.layout.activity_login)
public class LoginActivity extends RoboAccountAuthenticatorActivity {

    public final static String ARG_ACCOUNT_TYPE = "ACCOUNT_TYPE";
    public final static String ARG_AUTH_TYPE = "AUTH_TYPE";
    public final static String ARG_ACCOUNT_NAME = "ACCOUNT_NAME";
    public final static String ARG_IS_ADDING_NEW_ACCOUNT = "IS_ADDING_ACCOUNT";

    public static final String KEY_ERROR_MESSAGE = "ERR_MSG";

    public final static String PARAM_USER_PASS = "USER_PASS";

    private final int REQ_SIGNUP = 1;

    private final String TAG = "LoginActivity";



    @Override
    protected void onCreate(Bundle savedInstanceState) {


        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        getActionBar().hide();

        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new LoginFragment())
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    protected void FinishLogin(Intent intent){

    }



    public class LoginFragment extends RoboFragment {

        public static final String TAG = "LoginFragment";

        @InjectView(R.id.login_IVScroll)
        ScrollView m_logoScrollView;

        @InjectView(R.id.login_loginButton)
        Button m_loginButton;

        @InjectView(R.id.login_userId)
        EditText m_username;

        @InjectView(R.id.login_password)
        EditText m_password;

        @InjectView(R.id.login_error)
        TextView m_error;

        private String mAuthTokenType;
        private AccountManager mAccountManager;

        public LoginFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            return inflater.inflate(R.layout.fragment_login, container, false);
        }

        @Override
        public void onViewCreated(View view, Bundle savedInstanceState){
            super.onViewCreated(view, savedInstanceState);

            mAuthTokenType = getActivity().getIntent().getStringExtra(ARG_AUTH_TYPE);
            mAccountManager = AccountManager.get(getActivity());

            m_logoScrollView.setEnabled(false);

            m_loginButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final String username = m_username.getText().toString();
                    final String password = m_password.getText().toString();
                    final String accountType = getActivity().getIntent().getStringExtra(ARG_ACCOUNT_TYPE);
                    final UserLoginRequest loginRequest = new UserLoginRequest(username, password);

                    new AsyncTask<UserLoginRequest, Void, Intent>(){
                        private static final String TAG = "AuthUserTask";

                    protected Intent doInBackground(UserLoginRequest... request) {
                        String authToken = null;
                        Log.d(TAG, "Starting authentication");

                        Bundle data = new Bundle();
                        try {
                            AuthenticationService service = new AuthenticationService();
                            authToken = service.LoginUser(request[0]);

                            data.putString(AccountManager.KEY_ACCOUNT_NAME, username);
                            data.putString(AccountManager.KEY_ACCOUNT_TYPE, accountType);
                            data.putString(AccountManager.KEY_AUTHTOKEN, authToken);
                            data.putString(PARAM_USER_PASS, password);

                        } catch(AuthenticationService.UnauthorizedException exception){
                            Log.e(TAG, "User not authorized");

                            data.putString(KEY_ERROR_MESSAGE, exception.getMessage());
                        }

                        final Intent res = new Intent();
                        res.putExtras(data);
                        return res;
                    }


                    protected void onPostExecute(Intent intent){
                        Log.v(TAG, "Auth user task finished");

                        if (intent.hasExtra(KEY_ERROR_MESSAGE)) {
                            clearPassword();
                            m_error.setText(R.string.invalid_password);
                            m_error.setVisibility(View.VISIBLE);
                        } else {
                            m_error.setVisibility(View.GONE);
                            finishLogin(intent);
                        }
                    }
                    }.execute(loginRequest);
                }
            });
        }

        protected void clearPassword(){
             Handler mainHandler = new Handler(Looper.getMainLooper());

             mainHandler.post(new Runnable(){
                 @Override public void run(){
                   m_password.setText("");
                 }
             });

        }

        private void finishLogin(Intent intent) {
            Log.d( TAG , "finishLogin");

            String accountName = intent.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
            String accountPassword = intent.getStringExtra(PARAM_USER_PASS);
            final Account account = new Account(accountName, intent.getStringExtra(AccountManager.KEY_ACCOUNT_TYPE));

            if (getActivity().getIntent().getBooleanExtra(ARG_IS_ADDING_NEW_ACCOUNT, false)) {
                Log.d( TAG ,"> finishLogin > addAccountExplicitly");
                String authToken = intent.getStringExtra(AccountManager.KEY_AUTHTOKEN);
                String authTokenType = mAuthTokenType;

                // Creating the account on the device and setting the auth token we got
                // (Not setting the auth token will cause another call to the server to authenticate the user)
                mAccountManager.addAccountExplicitly(account, accountPassword, null);
                mAccountManager.setAuthToken(account, authTokenType, authToken);
            } else {
                Log.d(TAG," finishLogin > setPassword");
                mAccountManager.setPassword(account, accountPassword);
            }

            setAccountAuthenticatorResult(intent.getExtras());
            setResult(RESULT_OK, intent);
            finish();
        }


    }
}

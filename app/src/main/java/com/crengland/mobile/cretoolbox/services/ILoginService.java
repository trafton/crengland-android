package com.crengland.mobile.cretoolbox.services;

import com.crengland.mobile.cretoolbox.models.Trip;
import com.crengland.mobile.cretoolbox.models.UserLoginRequest;

import java.util.List;

import retrofit.client.Response;
import retrofit.http.GET;
import retrofit.http.Header;

/**
 * Created by aresix on 11/18/14.
 */
public interface ILoginService {
    @GET("/rest/rest/api/v1/trips")
    Response Login(@Header("Authorization")UserLoginRequest request) throws AuthenticationService.UnauthorizedException;
}


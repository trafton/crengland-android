package com.crengland.mobile.cretoolbox.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.crengland.mobile.cretoolbox.CREnglandAuthenticator;

/**
 * Created by aresix on 11/20/14.
 */
public class AccountAuthenticatorService extends Service {
    @Override
    public IBinder onBind(Intent intent) {
        CREnglandAuthenticator authenticator = new CREnglandAuthenticator(this);
        return authenticator.getIBinder();
    }
}

package com.crengland.mobile.cretoolbox;

/**
 * Created by aresix on 11/20/14.
 */
public class AccountGeneral {

    /**
     * Account type id
     */
    public static final String ACCOUNT_TYPE = "com.crengland.mobile.cretoolbox";

    /**
     * Account name
     */
    public static final String ACCOUNT_NAME = "CreToolbox";

    /**
     * Auth token types
     */
    public static final String AUTHTOKEN_TYPE_READ_ONLY = "Read only";
    public static final String AUTHTOKEN_TYPE_READ_ONLY_LABEL = "Read only access to an Cre Toolbox account";

    public static final String AUTHTOKEN_TYPE_FULL_ACCESS = "Full access";
    public static final String AUTHTOKEN_TYPE_FULL_ACCESS_LABEL = "Full access to an Cre Toolbox account";

   // public static final ServerAuthenticate sServerAuthenticate = new ParseComServerAuthenticate();
}